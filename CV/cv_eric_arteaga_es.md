# Curriculum Vitae - Eric Arteaga

No todo el que deambula está perdido

![Artorias|200](https://areajugones.sport.es/wp-content/uploads/2016/04/Artorias-Dark-Souls-1080x609.jpg.webp)

**Programador junior** con poca experiencia en el sector, pero mucha determinación por ello.

Si necesitas ponerte en contaco conmigo puedes escribirme un correo electrónico en *eartmar3010@g.educaand.es*

## Experiencia Laboral

* Pinche de cocina
  * 2020-2021 Las Tinajas, Aracena
  * 2022-2023 Stradina, Aracena
* programador Junior
  * 2022-2023 SNGULAR

## Títulos académicos

1. Grado Superior de **Desarrollo de Aplicaciones Multiplataforma**
2. Grado Superior de **Desarrollo de Aplicaciones Web**
Ambos obtenidos en el centro ["IES Velazquez"](https://iesvelazquez.org/ ) de Sevilla

## Habilidades

| Habilidad | Descripción | Experiencia |
|:------------|:-----------------|---------:|
| Java | Lenguaje de programación enfocado en el desarrollo de aplicaciones multiplataforma | :star: :star: :star: :star: |
| HTML | Lenguaje de marcas de hipertexto que actua como el esqueleto de una página web | :star: :star: :star: :star: |
| JavaScript | Lenguaje de programación enfocado en el desarrollo web que trabaja en el front-end | :star: :star: |
| CSS | Hoja de estilos que indica el aspecto que va a tener la estructura web HTML | :star: :star: :star: |
| PHP | Lenguaje de programación enfocado en el desarrollo web que trabaja con un servidor en el back-end | :star: :star: :star: |

## Ejemplo de código

```java
try {
            System.out.println("Comienzo del bloque try.");
            System.out.println("Intentando abrir el fichero.");
            FileReader fichero = new FileReader("texto.txt");
            BufferedReader br = new BufferedReader(fichero);
            String s = br.readLine();
            while (s != null) {
                System.out.println(s);
                s = br.readLine();
            }
            br.close();
            fichero.close();
            System.out.println("Fichero procesado.");
            System.out.println("Fin del bloque try.");
        } catch (FileNotFoundException ex) {
            System.err.println("No se ha encontrado el fichero.");
            System.err.println("La excepción ha sido capturada y nuestro programa no terminó repentinamente.");
            ex.printStackTrace();        
        } catch (IOException ex) {
            System.err.println("Al tratar con ficheros debemos capturar la excepción IOException.");
            
        }   finally {
            System.out.println("Este código se ejecutará siempre");
        }
        
        System.out.println("La vida continúa después de un bloque try-catch-finally");
```

## Tarjeta de visita

Puedes utilizar este JSON para guardar mis datos :satisfied:

```json
{
    "name": "Eric",
    "surame": "Arteaga Martín",
    "phone": [
        {
        "label": "work",
        "prefix": "+34",
        "number": 765876987
        }
    ],
    "email": "eartmar3010@g.educaand.es"
}
