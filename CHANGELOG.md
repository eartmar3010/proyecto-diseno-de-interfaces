# Changelog
Todos los cambios a este proyecto estará documentado en este fichero

## [0.1.0] 2022-12-12

### Añadido

- Carpeta CV con practica Curriculum Vitae

### Modificado

- README.md será utilizado como un índice

## [0.0.0] 2022-12-12

### Añadido

- .gitignore
- README.md